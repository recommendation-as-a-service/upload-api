const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const index = require('./routes/index');
const users = require('./routes/users');
const uploads = require('./routes/uploads');

const app = express();
app.use(cors());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.Promise = require('bluebird');
if (app.get('env') === 'development') {
    console.log("Connecting to Dev Mongo")
    mongoose.connect('mongodb://159.203.64.136', { server: { reconnectTries: Number.MAX_VALUE } });
} else {
    mongoose.connect('mongodb://upload-db', { server: { reconnectTries: Number.MAX_VALUE } });
}

app.use('/', index);
app.use('/users', users);
app.use('/uploads', uploads);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('error');
});

module.exports = app;