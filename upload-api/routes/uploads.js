var express = require('express');
var router = express.Router();
const User = require('../schemas/User.schema');
const Upload = require('../schemas/Upload.schema');
const mongoose = require('mongoose');
const aws = require('aws-sdk');
const multer = require('multer');
const amqp = require('amqplib/callback_api');

aws.config.loadFromPath('./aws.config.json');
const s3 = new aws.S3();

const amqpUrl = process.env.NODE_ENV === 'development' ? "amqp://159.203.64.136" : "amqp://rabbitmq";


router.get('/:userId', function(req, res, next) {
    Upload.find({ userId: req.params.userId }, {}, (err, uploads) => {
        if (err) {
            res.status(500).json({
                message: "Unable to connect to DB",
                error: err
            });
        } else if (uploads == null || uploads.length == 0) {
            res.status(404).json({
                message: "No uploads were found with that Id",
                userId: req.params.userId
            });
        } else {
            res.json({
                uploads: uploads,
                message: "Found uploads for that user!"
            });
        }
    });
});

router.post('/:userId', multer().any(), (req, res, next) => {
    const uploadId = new mongoose.Types.ObjectId();
    const fileName = uploadId + "-" + req.files[0].originalname;
    const uploadOptions = {
        Bucket: 'raas-final-project',
        ACL: 'public-read',
        Key: fileName,
        Body: req.files[0].buffer
    };

    s3.putObject(uploadOptions, (err, data) => {
        if (err) {
            res.status(500).json({
                message: "Unable to connect to S3",
                error: err
            });
        } else {
            Upload.create({
                userId: req.params.userId,
                algorithm: req.body.algorithm,
                k: req.body.k,
                fileName: fileName,
                url: "https://s3.us-east.amazonaws.com/raas-final-project/" + fileName,
                uploadId: uploadId,
                _id: new mongoose.Types.ObjectId()
            }, (err, upload) => {
                if (err) {
                    res.status(500).json({
                        message: "Unable to connect to DB",
                        error: err
                    });
                } else {
                    amqp.connect(amqpUrl, (err, connection) => {
                        console.log("Conected");
                        if (err) {
                            res.status(500).json({
                                message: "Unable to connect to RabbitMQ",
                                error: err
                            });
                        } else {
                            connection.createChannel((err, channel) => {
                                if (err) {
                                    res.status(500).json({
                                        message: "Unable to Create Channel",
                                        error: err
                                    });
                                } else {
                                    const message = Buffer.from(JSON.stringify({
                                        url: upload.url,
                                        userId: upload.userId,
                                        uploadId: upload.uploadId,
                                        algorithm: upload.algorithm,
                                        k: upload.k,
                                        fileName: upload.fileName
                                    }));
                                    const queue = 'recommender';
                                    channel.assertQueue(queue, { durable: false });
                                    channel.sendToQueue(queue, message);
                                    console.log("[ x ] Sent %s to queue", upload.url);
                                    res.json({
                                        details: "Send Data to Queue and stored it in DB",
                                        upload: upload,
                                        queueMessage: message.toJSON()
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

router.post('/train/:id', (req, res, next) => {
    Upload.findById(req.params.id, {}, (err, upload) => {
        if (err) {
            res.status(500).json({
                message: "There was an error finding a Upload",
                error: err
            });
        } else if (upload == null) {
            res.status(404).json({
                message: "No upload with that id was found",
                id: req.params.id
            });
        } else {
            console.log(req.body.algorithm);
            Upload.create({
                url: upload.url,
                userId: upload.userId,
                fileName: upload.fileName,
                algorithm: req.body.algorithm,
                k: req.body.k,
                uploadId: upload.uploadId,
                _id: new mongoose.Types.ObjectId()
            }, (err, newUpload) => {
                if (err) {
                    res.status(500).json({
                        message: "There was an error writing to the DB",
                        error: err
                    });
                } else {
                    amqp.connect(amqpUrl, (err, connection) => {
                        console.log("Conected");
                        if (err) {
                            res.status(500).json({
                                message: "Unable to connect to RabbitMQ",
                                error: err
                            });
                        } else {
                            connection.createChannel((err, channel) => {
                                if (err) {
                                    res.status(500).json({
                                        message: "Unable to Create Channel",
                                        error: err
                                    });
                                } else {
                                    const message = Buffer.from(JSON.stringify({
                                        url: newUpload.url,
                                        userId: newUpload.userId,
                                        uploadId: newUpload.uploadId,
                                        algorithm: newUpload.algorithm,
                                        k: newUpload.k,
                                        fileName: newUpload.fileName
                                    }));
                                    const queue = 'recommender';
                                    channel.assertQueue(queue, { durable: false });
                                    channel.sendToQueue(queue, message);
                                    console.log("[ x ] Sent %s to queue", newUpload.url);
                                    res.json({
                                        details: "Send Data to Queue and stored it in DB",
                                        upload: newUpload,
                                        queueMessage: message.toJSON()
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

module.exports = router;