var express = require('express');
var router = express.Router();
const User = require('../schemas/User.schema');
const mongoose = require('mongoose');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/signup', (req, res, next) => {
    User.create({
        email: req.body.email,
        password: req.body.password,
        _id: new mongoose.Types.ObjectId()
    }, (err, user) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(user);
        }
    });
});

router.post('/login', (req, res, next) => {
    User.findOne({ email: req.body.email }, {}, (err, user) => {
        if (err) {
            res.status(500).send(err);
        } else if (user == null) {
            res.status(404).send("Email not found");
        } else if (user.password == req.body.password) {
            res.json(user);
        } else {
            res.status(403).send("Invalid Password");
        }
    });
});

module.exports = router;