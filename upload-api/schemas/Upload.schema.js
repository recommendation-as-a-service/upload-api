const mongoose = require('mongoose');

module.exports = mongoose.model("Upload", mongoose.Schema({
    url: String,
    userId: String,
    algorithm: String,
    uploadId: String,
    k: Number,
    fileName: String,
    _id: String
}));